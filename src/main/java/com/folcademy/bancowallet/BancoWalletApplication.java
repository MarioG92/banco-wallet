package com.folcademy.bancowallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoWalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoWalletApplication.class, args);
	}

}
