package com.folcademy.bancowallet.models.entities;

import com.folcademy.bancowallet.models.enums.AccountType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity(name = "accounts")
public class AccountEntity {
    @Id
    private Long number;
    private String cbu;
    @Enumerated(EnumType.STRING)
    private AccountType type;

    public AccountEntity() {
    }

    public AccountEntity(Long number, String cbu, AccountType type) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
    }

    public Long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountType getType() {
        return type;
    }
}
