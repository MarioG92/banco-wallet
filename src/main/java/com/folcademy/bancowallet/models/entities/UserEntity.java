package com.folcademy.bancowallet.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {

    @Id
    private String dni;
    private String firstName;
    private String lastName;
    private String address;

    public UserEntity() {
    }

    public UserEntity(String dni, String firstName, String lastName, String address) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }
}
