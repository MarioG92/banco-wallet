package com.folcademy.bancowallet.models.enums;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}
